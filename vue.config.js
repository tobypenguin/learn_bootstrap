module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production'
      ? '/~cs62160247/learn_bootstrap/'
      : '/'
}
